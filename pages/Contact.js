import Section from '../components/shared/Section'
import { useForm } from 'react-hook-form'
import { Dialog, Transition } from '@headlessui/react'
import { send } from '@emailjs/browser'
import { useState, useEffect, Fragment } from 'react'
import { Facebook, Instagram } from '../public/icons/icons'

const containerStyles = 'flex flex-col w-72 mb-6'
const inputStyles =
  'bg-transparent border-b-2 border-white  placeholder:text-grey1 pt-2 pb-2 focus-visible:p-2 outline-1 outline-grey1 resize-none'

const Contact = () => {
  const { register, handleSubmit, watch, setValue } = useForm()

  const watchPhone = watch()

  useEffect(() => {
    console.log(watchPhone)
  }, [watchPhone])

  let [isOpen, setIsOpen] = useState(false)

  const convertPhone = (i) => {
    let trimmed = i.replaceAll(/\D/g, '')

    let areaCode = trimmed.substring(0, 3)
    let prefix = trimmed.substring(3, 6)
    let id = trimmed.substring(6, 10)

    let newPhone = areaCode
    if (prefix.length) newPhone += `-${prefix}`
    if (id.length) newPhone += `-${id}`

    console.log(newPhone)
    return newPhone
  }

  function closeModal() {
    setIsOpen(false)
  }

  function openModal() {
    setIsOpen(true)
  }

  const onSubmit = (data) => {
    console.log(data)
    send('service_rvcljj8', 'template_enyp9ki', data, 'nxapXtCrL2Qui0aqK')
      .then((response) => {
        openModal()
        console.log('SUCCESS!', response.status, response.text)
      })
      .catch((err) => {
        console.log('FAILED...', err)
      })
  }
  const onError = () =>
    alert('There was an issue with the form submission, please try again')

  return (
    <div id="contact">
      <Section title="Contact Me">
        <Transition appear show={isOpen} as={Fragment}>
          <Dialog as="div" className="relative z-10" onClose={closeModal}>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0">
              <div className="fixed inset-0 bg-black bg-opacity-25" />
            </Transition.Child>

            <div className="fixed inset-0 overflow-y-auto">
              <div className="flex min-h-full items-center justify-center p-4 text-center">
                <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0 scale-95"
                  enterTo="opacity-100 scale-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100 scale-100"
                  leaveTo="opacity-0 scale-95">
                  <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-pink1 p-6 text-center align-middle shadow-xl transition-all">
                    <Dialog.Title
                      as="h3"
                      className="text-lg font-medium leading-6 text-gray-900">
                      Your request has been sent
                    </Dialog.Title>
                    <div className="mt-2">
                      <p className="text-sm text-darkGrey1">
                        We will email you back with information regarding your
                        request, thanks!
                      </p>
                    </div>

                    <button
                      type="button"
                      className="bg-teal2 w-72 rounded-md p-2 mt-5 cursor-pointer hover:bg-teal1"
                      onClick={closeModal}>
                      Got it!
                    </button>
                  </Dialog.Panel>
                </Transition.Child>
              </div>
            </div>
          </Dialog>
        </Transition>
        <form
          onSubmit={handleSubmit(onSubmit, onError)}
          className="font-Cantarell">
          <div className={containerStyles}>
            <label htmlFor="email" className="">
              Email Address
            </label>
            <input
              type="email"
              id="email"
              placeholder="me@email.com"
              className={inputStyles}
              required
              {...register('email')}
            />
          </div>

          <div className={containerStyles}>
            <label htmlFor="name">Full Name</label>
            <input
              type="text"
              id="name"
              className={inputStyles}
              placeholder="Jane Doe"
              required
              {...register('name')}
            />
          </div>

          <div className={containerStyles}>
            <label htmlFor="phone">Phone #</label>
            <input
              type="tel"
              id="phone"
              name="phone"
              placeholder="123-456-7890"
              pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
              {...register('phone')}
              onChange={(ev) =>
                setValue('phone', convertPhone(ev.target.value))
              }
              className={inputStyles}
              required
            />
          </div>
          <div className={containerStyles}>
            <label htmlFor="date">Wedding or Event Date</label>
            <input
              type="date"
              id="date"
              placeholder="mo"
              className={inputStyles}
              {...register('date')}
            />
          </div>
          <div className={containerStyles}>
            <label htmlFor="location">Location</label>
            <input
              type="text"
              id="location"
              className={inputStyles}
              placeholder="Atlanta or Hearst Country Club"
              required
              {...register('location')}
            />
          </div>

          <div className={containerStyles}>
            <label htmlFor="services">
              Estimated Bridesmaid Services (for weddings)
            </label>
            <input
              type="number"
              id="services"
              className={inputStyles}
              placeholder="3"
              {...register('services')}
            />
          </div>

          <div className={containerStyles}>
            <label htmlFor="services">Additional Comments</label>
            <textarea
              type="text"
              id="services"
              className={inputStyles}
              rows="4"
              cols="50"
              placeholder="Anything you may need to add here..."
              {...register('additional')}
            />
          </div>

          <input
            type="submit"
            className="block m-auto bg-teal2 w-72 rounded-md p-2 mt-7 cursor-pointer hover:bg-teal1"
          />
        </form>
      </Section>
      <div className="flex justify-center pb-5">
        <a
          href="https://www.facebook.com/krcosmeticartistry"
          className="mr-2"
          target="_blank"
          rel="noopener noreferrer">
          <Facebook />
        </a>
        <a
          href="https://www.instagram.com/krcosmeticartistry"
          target="_blank"
          rel="noopener noreferrer">
          <Instagram />
        </a>
      </div>
    </div>
  )
}

export default Contact
