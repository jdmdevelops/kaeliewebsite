import Section from '../components/shared/Section'
import Content from '../components/shared/Content'

const Travel = () => (
  <Section title="Travel">
    <Content extra="For any on-location service, the first 30 miles from my location in King George, Va are on me! Thereafter, the cost is as follows: " />
    <div className="flex flex-col font-Cantarell items-center">
      <div className="text-lg">Miles over 30:</div>
      <div>1-10 | $20</div>
      <div>11-20 | $25</div>
      <div>21-30 | $30</div>
      <div>31-40 | $35</div>
      <div className="mt-5 italic">
        Distance exceeding these amounts will be calculated as 50 cents for each
        additional mile.
      </div>
    </div>
  </Section>
)

export default Travel
