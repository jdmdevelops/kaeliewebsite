import Image from 'next/image'
import { useState, useEffect } from 'react'
import Carousel from '../components/home/Carousel'
import pic4 from '../public/pics/4.jpg'
import { Menu, Transition } from '@headlessui/react'

import { ChevronUp, ChevronDown } from '../public/icons/icons'

const Dropdown = () => {
  // const [clicked, setClicked] = useState(false)

  const menuItemStyles = 'p-2 hover:border-b-2 border-teal2'
  return (
    <div className="fixed top-0 w-full z-10">
      <Menu className="border-b-4 border-pink1">
        {({ open }) => (
          <>
            <Menu.Button
              className={`flex justify-center p-2 bg-pink2 w-full border-b-4 border-pink1 outline-none`}>
              {open ? <ChevronUp /> : <ChevronDown />}
            </Menu.Button>

            <Transition
              show={open}
              enter="transition duration-200 ease-out"
              enterFrom="transform scale-95 opacity-0"
              enterTo="transform scale-100 opacity-100"
              leave="transition duration-100 ease-out"
              leaveFrom="transform scale-100 opacity-100"
              leaveTo="transform scale-95 opacity-0">
              <Menu.Items
                static
                className="flex flex-col items-center bg-pink2 p-2 border-b-4 border-pink1 font-Montserrat text-xl text-darkGrey1-400 relative bottom-1 outline-none">
                <Menu.Item className={menuItemStyles}>
                  {({ active }) => (
                    <a
                      className={`${active && 'border-b-2 border-teal2'}`}
                      href="#home">
                      HOME
                    </a>
                  )}
                </Menu.Item>
                <Menu.Item className={menuItemStyles}>
                  {({ active }) => (
                    <a
                      className={`${active && 'border-b-2 border-teal2'}`}
                      href="#about">
                      ABOUT
                    </a>
                  )}
                </Menu.Item>
                <Menu.Item className={menuItemStyles}>
                  {({ active }) => (
                    <a
                      className={`${active && 'border-b-2 border-teal2'}`}
                      href="#services">
                      SERVICES
                    </a>
                  )}
                </Menu.Item>
                <Menu.Item className={menuItemStyles}>
                  {({ active }) => (
                    <a
                      className={`${active && 'border-b-2 border-teal2'}`}
                      href="#contact">
                      CONTACT
                    </a>
                  )}
                </Menu.Item>
              </Menu.Items>
            </Transition>
          </>
        )}
      </Menu>
    </div>
  )
}

const Header = () => {
  return (
    <div className="pt-14" id="home">
      <Dropdown />
      <h1 className="flex justify-center  font-Cookie text-teal2 text-7xl leading-none">
        Kaelie Renae
      </h1>
      <h3 className=" flex justify-center font-Montserrat text-darkGrey1 text-2xl mb-5">
        COSMETIC ARTISTRY
      </h3>
    </div>
  )
}

const Home = () => {
  const [shown, setShown] = useState(false)
  const togglePopout = () => setShown(!shown)

  return (
    <div id="">
      <div className={shown ? 'blur-sm' : ''}>
        <Header togglePopout={togglePopout}></Header>
        <div className="flex flex-col items-center">
          <Image
            src={pic4}
            alt="Picture of the author"
            width={320}
            height={338}
            className="object-cover rounded-[30px]"></Image>

          <Carousel />
        </div>
      </div>
    </div>
  )
}

export default Home
