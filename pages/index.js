import Home from './Home'
import About from './About'
import Services from './Services'
import Contact from './Contact'
import Travel from './Travel'

const Index = () => {
  return (
    <>
      <Home />
      <About />
      <Services />
      <Travel />
      <Contact />
    </>
  )
}

export default Index
