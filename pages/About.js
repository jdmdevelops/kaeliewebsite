import Image from 'next/image'
import pic10 from '../public/pics/10.jpg'
import Section from '../components/shared/Section'
import Content from '../components/shared/Content'

const About = () => {
  return (
    <div className="" id="about">
      <Section
        img={
          <Image
            src={pic10}
            alt="Picture of Kaelie"
            objectFit="cover"
            width={340}
            height={350}
            className="rounded-[30px]"></Image>
        }
        title="About">
        <Content
          title={<span className="text-base">THE GIRL BEHIND THE BRUSH</span>}
          paragraph="Hello and welcome! I’m so glad you’re here. My name is Kaelie, and
            I’m an on-location makeup artist for weddings and special occasions
            of all kinds. I was born and raised in Frederickburg, Virginia, but
            willing to travel just about anywhere! For as long as I can
            rememeber, I have always had a passion for makeup. The best part is
            that it’s never the same, beacuase everyone has a unique look. That
            being said, I can’t wait to work with you and create something I
            never have before!"
        />
      </Section>
    </div>
  )
}

export default About
