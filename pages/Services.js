import Image from 'next/image'
import pic1 from '../public/pics/1.jpg'
import pic2 from '../public/pics/2.jpg'
import pic9 from '../public/pics/9.jpg'
import pic5 from '../public/pics/5.jpg'
import Section from '../components/shared/Section'
import Content from '../components/shared/Content'

const priceBoxStyles = 'bg-teal1 rounded-lg w-32'

const Services = () => (
  <div className="flex flex-col items-center" id="services">
    <Section
      title="Weddings"
      img={
        <Image
          src={pic1}
          alt="wedding pic"
          width={333}
          height={200}
          objectFit="cover"
          className="rounded-[30px]"
        />
      }>
      <Content
        title="1: TRIAL | $95"
        paragraph="Bridal trials are the perfect time for us to get acquainted and discuss your goals for your wedding-day look. You are more than welcome to bring pictures for inspiration, or if you aren’t quite sure what you’re going for yet or if the most makeup you’ve ever experienced is a little mascara, don’t worry! I’m here to help you through any questions or concerns you may have. This is the time for you to get used to your bridal look, and perfect it before the big day. Bridal trials are not required, but are highly recommended."
      />
    </Section>
    <Section
      img={
        <Image
          src={pic9}
          alt="wedding pic"
          width={333}
          height={200}
          objectFit="cover"
          className="rounded-[30px]"
        />
      }>
      <Content
        title="2: SAVE THE DATE | $115"
        paragraph="If you loved what you saw at your trial and are ready to book, a $115 deposit is required upon booking, which will be alloted toward your wedding day expenses. You will also be sent a virtual form to fill out and submit through email to nail down all the details.
        "
      />
    </Section>

    <Section
      img={
        <Image
          src={pic2}
          alt="wedding pic"
          width={333}
          height={200}
          objectFit="cover"
          className="rounded-[30px]"
        />
      }>
      <Content
        title="3: THE WEDDING DAY - BRIDE | $115"
        paragraph="When the big day has finally arrived, you will be sealed with setting spray after getting dolled up, so your look lasts all night long. The bride will also receive a complimentary lipstick touch-up kit to reapply as needed. You’ll be ready to dance, eat, and kiss without worries!"
      />
      <div className="mt-5"></div>
      <Content
        title="BRIDAL PARTY | $85"
        paragraph="Your bridal party, or any members receiving makeup services, will have a custom makeup application to their liking and will then be sealed with setting spray."
      />
    </Section>

    <Section
      title={<span className="text-6xl">Special Occasion</span>}
      img={
        <Image
          src={pic5}
          alt="wedding pic"
          width={333}
          height={200}
          objectFit="cover"
          className="rounded-[30px]"
        />
      }>
      <Content
        title="PROMS, EVENTS, BOUDOIR, AND SO MUCH MORE..."
        extra="For any service, you have the choice of either traditional, OR airbrush makeup application! Airbrush carries a $15 up-charge."
        paragraph="No matter the occasion, you deserve to look and feel your best! Services for photography, events, etc are $85 including false lashes and setting spray."
      />
    </Section>
  </div>
)

export default Services
