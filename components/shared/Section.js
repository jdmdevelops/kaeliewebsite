//Refactor into one big component that can take Title, Img?, and children in a box

const Section = ({ title, children, img }) => (
  <div className="flex flex-col items-center">
    <h1 className="font-Cookie text-teal2 text-7xl leading-none mb-5">
      {title}
    </h1>
    <div>{img}</div>
    <div
      className={`bg-pink2 p-6 text-darkGrey1 rounded-lg m-7 ${
        img ?? 'mt-0'
      } max-w-[340px]`}>
      {children}
    </div>
  </div>
)

export default Section
