const Content = ({ title, extra, paragraph, stars }) => {
  return (
    <div className="flex flex-col items-center">
      <h2 className="font-Montserrat text-xl text-center w-full">
        <span className="flex justify-center items-center ">
          {title} {stars && <span className="text-sm ml-1">{stars}</span>}
        </span>
      </h2>

      {title && <div className="w-[50px] h-[3px] bg-teal1 mb-2 mt-2"></div>}
      {extra && <div className="font-Cantarell italic pb-5">{extra}</div>}
      <p className="font-Cantarell ">{paragraph}</p>
    </div>
  )
}

export default Content
