import Circle from './Circle'
import Review from './Review'
import reviews from '../../data/reviews.json'
import { useEffect, useState } from 'react'
import { Transition } from '@headlessui/react'

const Carousel = () => {
  const [activeReview, setActiveReview] = useState(reviews[0])

  const reviewElements = reviews.map((r, i) => (
    <Transition
      key={i}
      show={activeReview.id == i ? true : false}
      enter="transition-opacity duration-500 transform"
      enterFrom="opacity-0"
      enterTo="opacity-100"
      leave="transition-opacity duration-1000 transform"
      leaveFrom="opacity-100"
      leaveTo="opacity-0">
      <div
        key={i}
        className="bg-pink2 p-5 text-darkGrey1 rounded-lg m-7 w-[320px] absolute">
        <Review title={r.name} paragraph={r.content} className="" />
      </div>
    </Transition>
  ))

  useEffect(() => {
    const id = setTimeout(
      () =>
        setActiveReview(
          reviews[
            activeReview.id < reviews.length - 1 ? activeReview.id + 1 : 0
          ]
        ),
      10000
    )
    return () => clearTimeout(id)
  }, [activeReview])

  return (
    <>
      <div className="flex flex-col w-96 h-96">
        <div className="">{reviewElements}</div>
        {/* <div className="flex justify-center mb-7">
          {reviews.map((r, i) => (
            <Circle
              key={i}
              handleClick={() => setActiveReview(reviews[i])}
              active={activeReview.id === i ? true : false}></Circle>
          ))}
        </div> */}
      </div>
    </>
  )
}

export default Carousel
