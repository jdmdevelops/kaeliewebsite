import Link from 'next/link'

const menuLinkStyle =
  'ml-5 pt-7 font-Montserrat font-medium text-darkGrey1 text-3xl'

const exitButtonStyle = 'w-14 h-1 bg-darkGrey1 rounded-full absolute'

const PopoutMenu = ({ onClick, shown }) => (
  <div
    className={`w-56 cursor-pointer bg-pink1 h-full absolute z-10 ${
      shown ? 'translate-x-0' : '-translate-x-full'
    } ease-in-out duration-200`}>
    <div className="pt-10 ml-5 mb-5 relative" onClick={onClick}>
      <div className={`${exitButtonStyle + ' rotateRight'}`} />
      <div className={`${exitButtonStyle + ' rotateLeft'}`} />
    </div>
    <div className={menuLinkStyle} onClick={onClick}>
      <Link href="#about">ABOUT</Link>
    </div>
    <div className={menuLinkStyle} onClick={onClick}>
      <Link href="#services">SERVICES</Link>
    </div>
    <div className={menuLinkStyle}>
      <Link href="">CONTACT</Link>
    </div>
  </div>
)

export default PopoutMenu
