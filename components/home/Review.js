import Content from '../shared/Content'
import { Transition } from '@headlessui/react'

const Review = ({ title, ...props }) => (
  <Content title={title} stars=" ★★★★★" {...props}></Content>
)

export default Review
