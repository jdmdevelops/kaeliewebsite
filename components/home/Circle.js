const Circle = ({ handleClick, active }) => {
  return (
    <div
      className={`w-[12px] h-[12px] ml-1 mr-1 ${
        active ? 'bg-teal2' : 'bg-teal1'
      }  rounded-full`}
      onClick={handleClick}></div>
  )
}

export default Circle
